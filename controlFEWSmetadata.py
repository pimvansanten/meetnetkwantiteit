# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 14:50:57 2018

@author: santpi01
"""

import os
import pandas as pd
import zipfile
from sqlalchemy import create_engine, text

FEWSmap = '//am131/data/Archive/export/'
## beheerregister data ophalen
#database verbinding (hiervoor is noodzakelijk dat er een Oracle instance op de PC draait.)
engine = create_engine('oracle+cx_oracle://raadpleeg:raadpleeg@AM009:1521/GPRD')
conn = engine.connect()
query1 = text('''SELECT * FROM   DAMO_S.WS_STUWAGGREGATIE''')
cursor = conn.execute(query1)
# output uitlezen en naar dataframe
result=[]
for row in cursor:
    d = dict(row)
    result.append(d.values())
BR = pd.DataFrame(result)  
BR.columns = d.keys()
BR['X'] = BR['shape'].apply(lambda x: x.SDO_POINT.X)
BR['Y'] = BR['shape'].apply(lambda x: x.SDO_POINT.Y)

#FEWS tabellen ophalen uit meest recente configuratie dump.
bestanden = []
datums = []
with os.scandir(FEWSmap) as it:
    for entry in it:
        if entry.name.endswith('zip'):
            bestanden.append(entry.name)
            datum = entry.name.split('_')[1]
            datums.append(float(datum))
max_value = max(datums)
max_index = datums.index(max_value)
laatstefile = bestanden[max_index]

# FEWS bestanden inlezen
with zipfile.ZipFile(FEWSmap+laatstefile) as z:
    # oppervlakte water locaties
    with z.open('Config/MapLayerFiles/opp_locaties.csv') as f:
        FEWS = pd.read_csv(f,sep=';')# niet hie rindex col zetten omdat er duplicaten inzitten en die moeten er eerst uit en dat gebeurt hieronder
        # Als er een einddatum is ingevuld is die regel niet actueel en kan verwijderd worden
        FEWS.drop(FEWS.loc[pd.notna(FEWS['EIND_DATUM'])].index, axis=0, inplace=True)
        # nu kan wel de index gezet worden.
        FEWS = FEWS.set_index('ID')
        FEWS['tabel']='opp_locaties'
    with z.open('Config/MapLayerFiles/Ellitrack.csv') as f:
        elli = pd.read_csv(f,sep=';',index_col=0)
    with z.open('Config/MapLayerFiles/Realsense.csv') as f:
        ireal= pd.read_csv(f,sep=';',index_col=1)

# controle op contructiehoogte en nieuwe stuwen
BR.index = BR['code']
#subset met alleen handbediende stuwen
BRsub = BR.loc[BR['soortregelbaarheid']=='handmatig',['hoogteconstructie','opmerking','ws_inwindatum','X','Y']].copy()
BRsub['hoogteconstructie'] = BRsub['hoogteconstructie'].apply(lambda x:pd.to_numeric(x))
#LOP stuwen eruit want die hebben we niet in FEWS
BRsub.drop(BRsub.loc[BRsub['opmerking'].str.contains('LOP stuw')==True].index,axis=0,inplace=True)
BRsub.drop('opmerking',axis=1,inplace=True)
BRsub.dropna(subset=['hoogteconstructie'], inplace = True)
BRsub = BRsub.join(FEWS['MEETPUNT'])# FEWS BC toevoegen
BRsub = BRsub.join(elli['MEETPUNT'],rsuffix='_elli')# FEWS BC toevoegen
BRsub = BRsub.join(ireal['MEETPUNT'],rsuffix='_ireal')# FEWS BC toevoegen
BRsub['MEETPUNT']=BRsub['MEETPUNT'].combine_first(BRsub['MEETPUNT_ireal'])
BRsub['MEETPUNT']=BRsub['MEETPUNT'].combine_first(BRsub['MEETPUNT_elli'])
BRsub['verschil']=BRsub['hoogteconstructie']-BRsub['MEETPUNT'] # verschil tussen FEWS en Beheerregister
BRCHECK = BRsub.loc[BRsub['verschil']!=0]
BRNEW = BRCHECK.loc[pd.isnull(BRCHECK['MEETPUNT'])].copy()
BRCHANGE = BRCHECK.dropna(subset=['MEETPUNT']).copy()

#als er verschillen zijn of iets nieuws is, dan mail versturen
if BRCHECK.shape[0]>0:
    import smtplib

    # Import the email modules we'll need
#    from email.message import EmailMessage
    
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    msg = MIMEMultipart('alternative')
#    msg = EmailMessage()
    text=''
    html=''
#    msg.set_content(text)
    if BRCHANGE.shape[0]>0:    
        #html versie
        html = html+"<html><head></head><body><H3>Stuwen met gewijzigde Constructiehooghte:</H3>"
        BRCHANGE.rename(columns={'MEETPUNT':'Oud','hoogteconstructie':'Nieuw'},inplace=True)
        html = html+BRCHANGE[['Oud','Nieuw','ws_inwindatum']].to_html()
        #text versie
        text=text+'Stuwen met gewijzigde Constructiehooghte:\n\n'
        for i,r in BRCHANGE.iterrows():
            text=text+i+': '+'{:.3f}'.format(r['Oud'])+' wordt '+'{:.2f}'.format(r['Nieuw'])+'\n'
    if BRNEW.shape[0]>0:    
        html = html+"""\
        <html>
            <head></head>
            <body>
            <H3>Nieuwe stuwen:</H3>
        """
        BRNEW[['X','Y']] = BRNEW[['X','Y']].astype(int)
        html = html+ BRNEW[['hoogteconstructie','X','Y']].to_html()
        
        text = text+'\n\nNieuwe stuwen:\n\n'
        for i,r in BRNEW.iterrows():
            text=text+i+': '+'{:.2f}'.format(r['hoogteconstructie'])+' '+'{:.0f}'.format(r['X'])+' '+'{:.0f}'.format(r['Y'])+'\n'

    html = html+"""\
    </BODY>
    </HTML>
    """
    msg['Subject'] = 'Er zijn constructiehoogtes gewijzigd'
    msg['From'] = 'hydromeetnet@aaenmaas.nl'
#    msg['To'] = 'pvansanten@aaenmaas.nl'
    msg['To'] = 'fvandelangenberg@aaenmaas.nl'
    
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via our own SMTP server.
    s = smtplib.SMTP('172.23.1.200')
#    s.sendmail('hydromeetnet@aaenmaas.nl', ['pvansanten@aaenmaas.nl','hydromeetnet@aaenmaas.nl'], msg.as_string())
    s.sendmail('hydromeetnet@aaenmaas.nl', ['fvandelangenberg@aaenmaas.nl','hydromeetnet@aaenmaas.nl'], msg.as_string())
    s.quit()




